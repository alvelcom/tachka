#ifndef __SHADER__
#define __SHADER__

#include "gl.hpp"

#include <memory>
#include <vector>
#include <map>

namespace Shader {
    class Shader {
        public:
            Shader(GLenum shaderType, const std::string &filepath);
            ~Shader();

            GLuint getId() const;

        private:
            GLuint m_id;
    };

    class Program {
        public:
            Program(std::initializer_list<const Shader *> shaders);
            ~Program();

            void use() const;
            void unuse() const;

            GLint getUniformLocation(const char *name) const;

            GLuint getId() const;

        private:
            GLuint m_id;
    };

    class Factory {
        public:
            Factory();
            ~Factory();

            const Shader  *newShader(GLenum shaderType, const std::string &filepath);
            const Program *newProgram(std::initializer_list<const Shader *> shaders);

        private:
            std::map<std::string, std::unique_ptr<Shader>> m_shaders;
            std::vector<std::unique_ptr<Program>> m_programs;
    };
}

#endif
