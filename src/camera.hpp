#ifndef __CAMERA__
#define __CAMERA__

#include "gl.hpp"

namespace Camera {
    class Camera {
        public:
            Vec3 m_pos, m_angle;

            Camera(Vec3 pos, Vec3 angle);

            const Vec3 &getPos() const;
            const Vec3 &getAngle() const;

            Mat4 getProjMat() const;
            Mat4 getViewMat() const;

    };
}
#endif
