#include "models/terrain.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>
#include <iostream>

#include "shader.hpp"

namespace Model {
    Terrain::Terrain(Shader::Factory *shaders) {
        auto vertex = shaders->newShader(
                GL_VERTEX_SHADER, "assets/shaders/pass.vert");
        auto fragment = shaders->newShader(
                GL_FRAGMENT_SHADER, "assets/shaders/red.frag");
        m_program = shaders->newProgram({vertex, fragment});

        m_projm = m_program->getUniformLocation("projMat");
        m_viewm = m_program->getUniformLocation("viewMat");
        m_modelm = m_program->getUniformLocation("modelMat");

        std::vector<GLfloat> verts;
        srand(10);
        for (int i = -9; i < 10; i++) 
            for (int k = -9; k < 10; k++) {
                verts.push_back(0.1f * i);
                verts.push_back(-.5f + (rand() % 100) / 1000.f);
                verts.push_back(0.1f * k);
            }

        m_vao.addArray(3, verts);

        std::vector<GLint> inds;
        for (int i = -9; i < 9; i++) 
            for (int k = -9; k < 9; k++) {
                const int pos = (i + 9) * 19 + (k + 9);
                inds.push_back(pos);
                inds.push_back(pos+1);
                inds.push_back(pos+19);

                inds.push_back(pos+1);
                inds.push_back(pos+20);
                inds.push_back(pos+19);
            }
        m_vao.addElementArray(inds);
    }

    void Terrain::draw(const Camera::Camera &camera) {
        m_program->use();

        Mat4 el;
        Mat4 modelm; // = glm::rotate(el, glm::radians(-55.f), {1.f, 0.f, 0.f});
        Mat4 viewm = camera.getViewMat();
        Mat4 projm = camera.getProjMat();

        glUniformMatrix4fv(m_modelm, 1, GL_FALSE, glm::value_ptr(modelm));
        glUniformMatrix4fv(m_viewm,  1, GL_FALSE, glm::value_ptr(viewm));
        glUniformMatrix4fv(m_projm,  1, GL_FALSE, glm::value_ptr(projm));

        m_vao.bind();

        glDrawElements(GL_TRIANGLES, 19 * 19 * 6, GL_UNSIGNED_INT, 0);
        //printf("glDrawArrays = %x\n", glGetError());

        m_vao.unbind();
        m_program->use();
    }
}
