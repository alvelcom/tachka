#ifndef __RENDER_TERRAIN_
#define __RENDER_TERRAIN_

#include "camera.hpp"
#include "shader.hpp"
#include "vao.hpp"

#include <memory>


namespace Model {

    class Terrain {
        public:
            Terrain(Shader::Factory *shaders);
            void draw(const Camera::Camera &camera);

        private:
            VAO m_vao;
            const Shader::Program *m_program;

            GLint m_projm, m_viewm, m_modelm;
    };
}

#endif
