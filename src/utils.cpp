#include "utils.hpp"
#include <iostream>
#include <sstream>
#include <fstream>

namespace Utils {

    std::string readFile(const std::string& path) {
          std::ostringstream buf;
          std::ifstream input (path.c_str());
          buf << input.rdbuf();
          return buf.str();
    }
}
