#ifndef __VAO__
#define __VAO__

#include <vector>
#include "gl.hpp"

class VAO {
    public:
        VAO();
        virtual ~VAO();

        void bind() const;
        void unbind() const;

        GLuint addArray(int dim, const std::vector<GLfloat> &data);
        GLuint addElementArray(const std::vector<GLint> &data);

    private:
        GLuint m_vao;
        std::vector<GLuint> m_buffers;
};

#endif
