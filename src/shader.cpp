#include "shader.hpp"

#include <iostream>

#include "utils.hpp"

namespace Shader {
    Shader::Shader(GLenum shaderType, const std::string &filepath) {
        auto content = Utils::readFile(filepath);
        const char *content_c = content.c_str();

		m_id = glCreateShader(shaderType);
		glShaderSource(m_id, 1, &content_c, nullptr);
		glCompileShader(m_id);

		// Check for compile time errors
		GLint success;
		GLchar infoLog[512];
		glGetShaderiv(m_id, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(m_id, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        }
    }

    Shader::~Shader() {
		glDeleteShader(m_id);
    }

    GLuint Shader::getId() const {
        return m_id;
    }


    Program::Program(const std::initializer_list<const Shader *> shaders) {
        m_id = glCreateProgram();
        for (auto shader: shaders) {
            glAttachShader(m_id, shader->getId());
        }

        GLint success;
        GLchar infoLog[512];

        glLinkProgram(m_id);
        glGetProgramiv(m_id, GL_LINK_STATUS, &success);
        if (!success) {
            glGetProgramInfoLog(m_id, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
        }
    }

    Program::~Program() {
		glDeleteProgram(m_id);
    }

    void  Program::use() const {
        glUseProgram(m_id);
    }

    void  Program::unuse() const {
        glUseProgram(0);
    }

    GLint Program::getUniformLocation(const char *name) const {
        return glGetUniformLocation(m_id, name);
    }

    GLuint Program::getId() const {
        return m_id;
    }

    // Factory
    Factory::Factory() {}
    Factory::~Factory() {}

    const Shader *Factory::newShader(GLenum shaderType, const std::string &filepath) {
        auto it = m_shaders.find(filepath);
        if (it != m_shaders.end()) {
            std::cerr << "Loading shader " << filepath << " from cache." << std::endl;
            return it->second.get();
        } else {
            std::cerr << "Loading shader " << filepath << " from file... ";
            auto r = m_shaders.emplace(filepath,
                    std::make_unique<Shader>(shaderType, filepath));
            std::cerr << "done." << std::endl;
            return r.first->second.get();
        }
    }

    const Program *Factory::newProgram(std::initializer_list<const Shader *> shaders) {
        m_programs.emplace_back(std::make_unique<Program>(shaders));
        return m_programs.back().get();
    }
}

