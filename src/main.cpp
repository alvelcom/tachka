#include <iostream>

#include "gl.hpp"

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

#include "scene.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

int main()
{
    sf::ContextSettings settings;
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 4;
    settings.majorVersion = 3;
    settings.minorVersion = 3;
    settings.attributeFlags = sf::ContextSettings::Debug;

    sf::Window window(sf::VideoMode(800, 600), "SFML::OpenGL", sf::Style::Default, settings);

    auto mode = sf::VideoMode::getDesktopMode();
    window.setPosition({static_cast<int>(mode.width - window.getSize().x), 0});
    window.setFramerateLimit(30);

    window.setActive(true);
    window.setVerticalSyncEnabled(true);

	GLuint test;
	glViewport(0, 0, window.getSize().x, window.getSize().y);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    Scene scene;

    bool isOpen = true;
    while (window.isOpen() && isOpen)
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::Resized)
                glViewport(0, 0, event.size.width, event.size.height);
            else if (event.type == sf::Event::KeyPressed) {
                switch (event.key.code) {
                    case sf::Keyboard::Escape: isOpen = false; break;
                    case sf::Keyboard::W: scene.getCamera().m_pos.z -= 0.1; break;
                    case sf::Keyboard::S: scene.getCamera().m_pos.z += 0.1; break;
                    case sf::Keyboard::A: scene.getCamera().m_angle.y -= PI/50; break;
                    case sf::Keyboard::D: scene.getCamera().m_angle.y += PI/50; break;
                    default: break;
                }
            }
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        scene.draw();
        window.display();
    }

    return 0;
}
