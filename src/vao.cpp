#include "vao.hpp"

#include <cstdio>


VAO::VAO() {
    glGenVertexArrays(1, &m_vao);
}

VAO::~VAO() {
    glDeleteBuffers(m_buffers.size(), m_buffers.data());
    glDeleteVertexArrays(1, &m_vao);
}

void VAO::bind() const {
    glBindVertexArray(m_vao);
    glEnableVertexAttribArray(0);
}

void VAO::unbind() const {
    glBindVertexArray(0);
}

GLuint VAO::addArray(int dim, const std::vector<GLfloat> &data) {
    glBindVertexArray(m_vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);

    m_buffers.push_back(vbo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER,
            data.size() * sizeof(data[0]),
            data.data(),
            GL_STATIC_DRAW);

    glVertexAttribPointer(0, dim, GL_FLOAT, GL_FALSE, dim * sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return m_buffers.size();
}

GLuint VAO::addElementArray(const std::vector<GLint> &data) {
    glBindVertexArray(m_vao);

    GLuint ebo;
    glGenBuffers(1, &ebo);

    m_buffers.push_back(ebo);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            data.size() * sizeof(data[0]),
            data.data(),
            GL_STATIC_DRAW);

    //glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return m_buffers.size();
}
