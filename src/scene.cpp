#include "scene.hpp"

#include "models/terrain.hpp"

Scene::Scene()
: m_camera({0, 0, 3}, {0, 0, 0})
, m_shaders(std::make_unique<Shader::Factory>())
, m_terrain(std::make_unique<Model::Terrain>(m_shaders.get()))
{
}

Scene::~Scene() {
}

Camera::Camera &Scene::getCamera() {
    return m_camera;
}

void Scene::draw() {
    m_terrain->draw(m_camera);
}
