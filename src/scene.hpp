#ifndef __SCENE__
#define __SCENE__

#include "camera.hpp"
#include <memory>

namespace Shader {
    class Factory;
};

namespace Model {
    class Terrain;
};

class Scene {
    public:
        Scene();
        ~Scene();

        Camera::Camera &getCamera();

        void draw();

    private:
        Camera::Camera m_camera;

        std::unique_ptr<Shader::Factory> m_shaders;
        std::unique_ptr<Model::Terrain>  m_terrain;
        
};

#endif
