#ifndef __UTILS__
#define __UTILS__

#include <string>

namespace Utils {
    std::string readFile(const std::string& path);
}

#endif
