#include "camera.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace Camera {
    Camera::Camera(Vec3 pos, Vec3 angle)
        : m_pos(pos)
        , m_angle(angle)
    {}

    const Vec3 &Camera::getPos() const {
        return m_pos;
    }

    const Vec3 &Camera::getAngle() const {
        return m_angle;
    }

    Mat4 Camera::getProjMat() const {
        Mat4 matrix;
        //return matrix;
        //return glm::infinitePerspective(3.14f / 3, 800.0f/600.0f, 0.1f);
        return glm::perspective(3.14f / 3, 800.0f/600.0f, 0.1f, 100.0f);
    }

    Mat4 Camera::getViewMat() const {
        Mat4 matrix;

        matrix = glm::rotate(matrix, m_angle.x, {1, 0, 0});
        matrix = glm::rotate(matrix, m_angle.y, {0, 1, 0});
        matrix = glm::rotate(matrix, m_angle.z, {0, 0, 1});
        matrix = glm::translate(matrix, -m_pos);

        return matrix;
    }

}
