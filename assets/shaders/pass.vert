#version 330 core

layout (location = 0) in vec3 position;

out vec3 pass_position;

uniform mat4 projMat;
uniform mat4 viewMat;
uniform mat4 modelMat;

void main()
{
    vec4 tmp = projMat * viewMat * modelMat * vec4(position, 1.0f);
    pass_position = position;
    gl_Position = tmp;
}
