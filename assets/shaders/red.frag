#version 330 core

in vec3 pass_position;
out vec4 color;

void main()
{
    color = vec4(pass_position.xyz + 0.5, 1.0f);
}
