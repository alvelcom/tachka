OBJS = $(shell cd src && find * -name '*.cpp' | sed 's!\(.*\).cpp$$!build/\1.o!')

.PHONY: all build clean
all: build main

build:
	@mkdir -p build
	@cd src && find * -type d -exec mkdir -vp ../build/\{} \;

clean:
	rm -rf build

###
main: $(OBJS)
	clang++ -g -o $@ $^ -lsfml-window -lsfml-system -lGL

build/%.o: src/%.cpp
	clang++ -std=c++14 -I src -g -c -o $@ $^


